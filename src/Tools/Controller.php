<?php

namespace Tools;

abstract class Controller
{
    protected $context;

    public function __construct(Context $context)
    {
        $this->context = $context;
    }

    public function redirect($uri)
    {
        $response = new Response();
        $response->setStatus(302);
        $response->setRedirect($uri);

        return $response;
    }

    public function getFactory()
    {
        $class = get_class($this);
        $class = preg_replace('~^Controllers\\\(Front|Back)end~', 'Factories', $class);

        return new $class();
    }

    public function getEntity()
    {
        parse_str($this->context->getQuery());

        if (!isset($id)) {
            throw new \RuntimeException('Missing parameter');
        }
        
        $factory = $this->getFactory();
        if (!$entity = $factory->getById($id)) {
            throw new \RuntimeException('Entity not found');
        }

        return $entity;
    }
}