<?php

namespace Tools;

class View
{
    private $path;

    protected $template;
    protected $data;
    protected $params;

    public function __construct($template, array $data=[], array $params=[])
    {
        $this->path = ROOT_PATH . '/views';

        $this->template = $template;
        $this->data = $data;
        $this->params = $params;
    }

    public function setLayout($layout)
    {
        $this->params['layout'] = $layout;
    }

    public function render()
    {
        // Default values
        $layout = isset($this->params['layout']) ? $this->params['layout'] : 'frontend';
        $format = isset($this->params['format']) ? $this->params['format'] : 'html';

        $view_path = $this->path . '/' . $this->template . '.' . $format;

        if (!file_exists($view_path)) {
            throw new \Exception('Missing view ' . $view_path);
        }

        // capture view output
        ob_start();
        extract($this->data, EXTR_SKIP);
        include $view_path;
        $output = ob_get_clean();

        if ($layout) {
            $layout_path = $this->path . '/layouts/' . $layout . '.' . $format;
            if (!file_exists($layout_path)) {
                throw new \Exception('Missing layout ' . $layout_path);
            }
            ob_start();
            $session = Session::getInstance();
            $content = $output;
            include $layout_path;
            $output = ob_get_clean();
        }

        return $output;
    }
}