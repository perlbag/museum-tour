<?php

namespace Factories;

use Tools\Factory;

use Entities\Category as Entity;

class Category extends Factory
{
	protected $table = 'category';

    public function getRoots()
    {
        $sql = 'SELECT * FROM ' . $this->table . ' WHERE ISNULL(parent_id) ORDER BY position';

        $stmt = $this->db->query($sql, \PDO::FETCH_CLASS, '\\Entities\\' . ucfirst($this->table));

        return $stmt->fetchAll();
    }

    public function getTree($list=null)
    {
        if (null === $list) {
            $list = $this->getRoots();    
        }
        
        $tree = [];
        foreach ($list as $node) {
            $branch = $node->toArray();
            if ($children = $this->collectChildren($node, false)) {
                $branch['children'] = $this->getTree($children);
            }
            $tree[] = $branch;
        }

        return $tree;
    }

    public function getPath(Entity $entity)
    {
        if (!$entity->getParent_id()) {
            return [];
        }

        $sql = 'SELECT * FROM ' . $this->table . ' WHERE id=?';

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(1, $entity->getParent_id(), \PDO::PARAM_INT);
        $stmt->execute();

        $res = $stmt->fetchAll(\PDO::FETCH_CLASS, '\\Entities\\' . ucfirst($this->table));

        return array_merge($this->getPath($res[0]), $res);
    }

    public function getMuseums(Entity $category)
    {
        $children = $this->collectChildren($category);

        $list = [$category->getId()];
        foreach ($children as $child) {
            $list[] = $child->getId();
        }

        $marks = array_fill(0, count($list), '?');

        $sql = 'SELECT DISTINCT m.* 
                FROM museum m 
                JOIN museum_has_category link ON m.id=link.museum_id
                WHERE link.category_id IN (' . implode(',', $marks) . ')';

        $stmt = $this->db->prepare($sql);
        $key = 1;
        foreach ($list as $id) {
            $stmt->bindValue($key++, $id, \PDO::PARAM_INT);    
        }
        
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_CLASS, '\\Entities\\Museum');
    }

    public function collectChildren(Entity $category, $deep=true)
    {
        $sql = 'SELECT * FROM ' . $this->table . ' WHERE parent_id=? ORDER BY position';

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(1, $category->getId(), \PDO::PARAM_INT);
        $stmt->execute();

        if (!$res = $stmt->fetchAll(\PDO::FETCH_CLASS, '\\Entities\\' . ucfirst($this->table))) {
            return [];
        }
        $return = $res;
        
        if ($deep) {
            foreach ($res as $child) {
                $return = array_merge($return, $this->collectChildren($child, $deep));
            }    
        }

        return $return;
    }

    public function deleteLinks(Entity $entity)
    {
        $return = true;

        $children = $this->collectChildren($entity, false);
        foreach ($children as $child) {
            $return &= $this->delete($child);
        }

        $sql = 'DELETE FROM museum_has_category WHERE category_id=?';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(1, $entity->getId(), \PDO::PARAM_INT);
        $return &= $stmt->execute();

        return $return;
    }
}