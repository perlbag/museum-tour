<?php

function out($str, $quote=ENT_QUOTES, $encoding='UTF-8')
{
    return htmlentities($str, $quote, $encoding);
}

function formtree($tree, $params=[])
{
    if (!$tree) return '';

    if (!isset($params['type'])) {
        $params['type'] = 'radio';
    }

    $output = ['<ul>'];
    foreach ($tree as $branch) {
        
        if ('radio' == $params['type']) {
            $checked = isset($params['selected']['parent_id']) && $branch['id'] == $params['selected']['parent_id'];
            $disabled = isset($params['selected']['id']) && $branch['id'] == $params['selected']['id'];
        } else {
            $checked = isset($params['selected']['categories']) && in_array($branch['id'], $params['selected']['categories']);
            $disabled = false;
        }
        
        if (!$disabled && isset($params['frozen'])) {
            $disabled = $params['frozen'];
        }

        $output[] = sprintf('<li><label><input type="%s" name="%s" value="%d"%s%s> %s</label>', 
                        out($params['type']),
                        out($params['name']),
                        $branch['id'],
                        $checked ? ' checked="checked"' : '',
                        $disabled ? ' disabled="disabled"' : '',
                        out($branch['label'])
                    );

        if (isset($branch['children'])) {
            $output[] = formtree($branch['children'], array_merge($params, ['frozen' => $disabled]));
        }

        $output[] = '</li>';
    }
    $output[] = '</ul>';

    return implode("\n", $output);
}