<?php

namespace Entities;

use Tools\Entity;

class Category extends Entity
{
	protected $id;
	protected $parent_id;
	protected $label;
	protected $description;
}