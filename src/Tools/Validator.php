<?php

namespace Tools;

use Tools\Entity;

abstract class Validator
{
    protected $errors = [];
    protected $allowed = [];
    protected $required = [];
    protected $data;
    protected $entity;

    public function __construct(array $data, Entity $entity)
    {
        $data = array_intersect_key($data, array_flip($this->allowed));
        $this->data = array_filter($data);
        $this->entity = $entity;
    }

    public function isValid()
    {
        foreach ($this->required as $required => $msg) {
            if (!isset($this->data[$required])) {
                $this->errors[$required] = $msg;
            }
        }

        return empty($this->errors);
    }

    public function getData()
    {
        $data = $this->data;
        
        foreach ($this->allowed as $field) {
            if (!isset($data[$field])) {
                $data[$field] = 'NULL';
            }
        }

        return $data;
    }

    public function setData(array $data)
    {
        $this->data = $data;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getEntity()
    {
        return $this->entity;
    }
}