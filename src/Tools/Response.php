<?php

namespace Tools;

class Response
{
    protected $status = 200;
    protected $body;

    public function send()
    {
        switch ($this->status) {
            case 301:
            case 302:
                header('Location: ' . ($this->redirect ?: '/'), $this->status);
                exit();
            break;
            case 200:
            default:
                echo $this->body;
            break;
        }
    }

    public function __call($method, $args)
    {
        switch (true) {
            case 0 === strpos($method, 'set'):
                $key = strtolower(substr($method, 3));
                $this->$key = $args[0];
            break;
            default:
                throw new Exception('Invalid method: ' . $method, 500);
        }
    }
}
