SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';


-- -----------------------------------------------------
-- Table `museum`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `museum` ;

CREATE  TABLE IF NOT EXISTS `museum` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `label` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `category` ;

CREATE  TABLE IF NOT EXISTS `category` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `parent_id` INT NULL ,
  `label` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL ,
  `position` INT NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_category_2` (`parent_id` ASC) ,
  INDEX `idx_position` (`parent_id` ASC, `position` ASC) ,
  CONSTRAINT `fk_category_2`
    FOREIGN KEY (`parent_id` )
    REFERENCES `category` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `museum_has_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `museum_has_category` ;

CREATE  TABLE IF NOT EXISTS `museum_has_category` (
  `museum_id` INT NOT NULL ,
  `category_id` INT NOT NULL ,
  PRIMARY KEY (`museum_id`, `category_id`) ,
  INDEX `fk_museum_has_category_category1` (`category_id` ASC) ,
  INDEX `fk_museum_has_category_museum1` (`museum_id` ASC) ,
  INDEX `idx_position` (`category_id` ASC) ,
  CONSTRAINT `fk_museum_has_category_museum1`
    FOREIGN KEY (`museum_id` )
    REFERENCES `museum` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_museum_has_category_category1`
    FOREIGN KEY (`category_id` )
    REFERENCES `category` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
