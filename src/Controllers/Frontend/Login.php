<?php

namespace Controllers\Frontend;

use Tools\Controller;
use Tools\Response;
use Tools\View;
use Tools\Session;

class Login extends Controller
{
    public function indexAction()
    {
        $response = new Response();

        $error = null;

        if ('POST' == $_SERVER['REQUEST_METHOD']) {
            $login = $_POST['login'];
            $password = $_POST['password'];

            if (0 === strcmp($login, 'admin') && 0 === strcmp($password, 'mtour44')) {
                $session = Session::getInstance();
                $session->setCredentials();
                $response->setStatus(302);
                $response->setRedirect('/backend');

                return $response;
            }

            $error = 'Invalid credentials';
        }

        $view = new View('frontend/login/index', ['error'=>$error]);

        $response->setBody($view->render());

        return $response;
    }

    public function closeAction()
    {
        $session = Session::getInstance();
        $session->logout();

        $response = new Response();
        $response->setStatus(302);

        return $response;
    }
}