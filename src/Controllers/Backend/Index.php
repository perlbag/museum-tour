<?php

namespace Controllers\Backend;

use Tools\Controller;
use Tools\View;
use Tools\Response;

class Index extends Controller
{
    public function indexAction()
    {
        $view = new View('backend/index/index');
        $view->setLayout('backend');

        $response = new Response();
        $response->setBody($view->render());

        return $response;
    }
}