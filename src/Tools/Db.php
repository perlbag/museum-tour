<?php

namespace Tools;

class Db
{
    protected static $instance;

    protected $dbhost = 'localhost';
    protected $dbname = 'museum_tour';
    protected $dbuser = 'root';
    protected $dbpass = '';
    protected $dbopts =  [
        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "UTF8"',
    ];
    
    public function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }

    protected function __construct()
    {
        $dsn = sprintf('mysql:dbname=%s;host=%s', $this->dbname, $this->dbhost);
        $this->pdo = new \PDO($dsn, $this->dbuser, $this->dbpass, $this->dbopts);
    }

    public function __call($method, $args)
    {
        return call_user_func_array([$this->pdo, $method], $args);
    }
}
