<?php

// Welcome back to "flat" handmade PHP :)

define('ROOT_PATH', __DIR__ . '/..');

$loader = require ROOT_PATH . '/vendor/autoload.php';

try {
    $context = new Tools\Context();
    $response = $context->getResponse();
    $response->send();
} catch (Exception $e) {
    switch ($e->getCode()) {
        case 401:
            $context = new Tools\Context('/login');
            $response = $context->getResponse();
            $response->send();
            break;
        case 404:
            die('Page not found');
        default:
            die('Unhandled error: ' . $e->getMessage());
    }    
}
