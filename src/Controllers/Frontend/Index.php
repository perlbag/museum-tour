<?php

namespace Controllers\Frontend;

use Tools\Controller;
use Tools\View;
use Tools\Response;

class Index extends Controller
{
	public function indexAction()
	{
        $view = new View('frontend/index/index');

		$response = new Response();
		$response->setBody($view->render());

		return $response;
	}
}