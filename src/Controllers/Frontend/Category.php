<?php

namespace Controllers\Frontend;

use Tools\Controller;
use Tools\View;
use Tools\Context;
use Tools\Response;

use Factories\Category as Factory;

class Category extends Controller
{
    public function indexAction()
    {
        // Get all roots
        $factory = new Factory();
        $roots = $factory->getRoots();

        $view = new View('frontend/category/index', ['roots'=>$roots]);

        $response = new Response();
        $response->setBody($view->render());

        return $response;
    }

    public function viewAction()
    {
        parse_str($this->context->getQuery());

        if (!isset($id)) {
            throw new \RuntimeException('Missing parameter');
        }

        $factory = new Factory();
        $entity = $factory->getById($id);

        if (null === $entity) {
            throw new \Exception('Category not found', 404);
        }

        $path = $factory->getPath($entity);
        $museums = $factory->getMuseums($entity);
        $children = $factory->collectChildren($entity, false);

        $view = new View('frontend/category/view', ['item'=>$entity, 'path'=>$path, 'museums'=>$museums, 'children'=>$children]);

        $response = new Response();
        $response->setBody($view->render());

        return $response;
    }
}
