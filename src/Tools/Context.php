<?php

namespace Tools;

class Context
{
    protected $path;
    protected $query;

    public function __construct($uri=null) 
    {
        if (null === $uri) {
            $uri = $_SERVER['REQUEST_URI'];
        }

        $parts = parse_url($uri);

        $this->path = $parts['path'];
        $this->query = isset($parts['query']) ? $parts['query'] : null;
    }

    public function isRestricted() 
    {
        return 0 === strpos($this->path, '/backend');
    }

    public function getResponse()
    {
        $parts = explode('/', trim($this->path, '/'));

        $module = 'frontend';

        if ($this->isRestricted()) {
            // Restricted area / check permissions
            $session = Session::getInstance();
            if (!$session->hasCredentials()) {
                throw new \Exception('Please log in', 401);
            }
            $module = array_shift($parts);
        }

        $controller = array_shift($parts);
        if (empty($controller)) {
            $controller = 'index';
        }

        $action = array_shift($parts);
        if (empty($action)) {
            $action = 'index';
        }

        $action .= 'Action';

        $class = '\\Controllers\\' . ucfirst($module) . '\\' . ucfirst($controller);
        $process = new $class($this);

        if (!method_exists($process, $action)) {
            throw new \Exception('Page not found', 404);
        }

        return $process->$action();
    }

    public function __call($method, $args)
    {
        switch (true) {
            case 0 === strpos($method, 'get'):
                $key = strtolower(substr($method, 3));
                return isset($this->$key) ? $this->$key : null;
        }
    }
}
