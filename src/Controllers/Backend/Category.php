<?php

namespace Controllers\Backend;

use Tools\Controller;
use Tools\View;
use Tools\Response;

use Validators\Category as Validator;

class Category extends Controller
{
    public function indexAction()
    {
        $list = $this->getFactory()->getAll();

        $view = new View('backend/category/index', ['list'=>$list]);
        $view->setLayout('backend');

        $response = new Response();
        $response->setBody($view->render());

        return $response;
    }

    public function createAction()
    {
        return $this->form($this->getFactory()->create());
    }

    public function updateAction()
    {
        return $this->form($this->getEntity());
    }

    public function deleteAction()
    {
        $entity = $this->getEntity();

        $this->getFactory()->delete($entity);

        return $this->redirect('/backend/category');
    }

    protected function form($entity)
    {
        $vars = $entity->toArray();
        
        $errors = [];
        
        if ('POST' === $_SERVER['REQUEST_METHOD']) {
            $vars = array_merge($vars, $_POST['category']);
            $validator = new Validator($vars, $entity);
            if ($validator->isValid()) {
                $this->getFactory()->save($validator);
                
                return $this->redirect('/backend/category');    
            }

            $errors = $validator->getErrors();
        }
        
        $tree = $this->getFactory()->getTree();

        $view = new View('/backend/category/form', ['tree'=>$tree, 'vars'=>$vars, 'errors'=>$errors]);
        $view->setLayout('backend');

        $response = new Response();        
        $response->setBody($view->render());

        return $response;
    }
}