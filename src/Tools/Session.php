<?php

namespace Tools;


class Session
{
    protected static $instance;

    public function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function __construct()
    {
        session_start();
    }

    public function setCredentials()
    {
        $_SESSION['credentials'] = true;
    }

    public function hasCredentials()
    {
        return isset($_SESSION['credentials']);
    }

    public function logout()
    {
        unset($_SESSION['credentials']);
    }
}
