<?php

namespace Tools;

abstract class Factory
{	
	protected $db;

	public function __construct(Db $db=null)
	{
		if (null === $db) {
			$db = Db::getInstance();
		}

		$this->db = $db;
	}

	public function getById($id)
	{
		$sql = 'SELECT * FROM ' . $this->table . ' WHERE id=?';

		$stmt = $this->db->prepare($sql);
		$stmt->bindValue(1, $id, \PDO::PARAM_INT);
		$stmt->execute();

		if (!$res = $stmt->fetchAll(\PDO::FETCH_CLASS, '\\Entities\\' . ucfirst($this->table))) {
			if (false === $res) {
				throw new \RuntimeException('Invalid PDO statement');
			}
			return null;
		}

		if (count($res) > 1) {
			throw new \LogicalException('WTF?');
		}

		return current($res);
	}


    public function getAll()
    {
        $sql = 'SELECT * FROM ' . $this->table;

        $stmt = $this->db->query($sql, \PDO::FETCH_CLASS, '\\Entities\\' . ucfirst($this->table));

        return $stmt->fetchAll();
    }

	public function create()
	{
		$class = get_class($this);
		$class = preg_replace('~^Factories~', 'Entities', $class);

		return new $class();
	}

    public function save(Validator $validator)
    {
        $data = $validator->getData();
        $fields = [];
        foreach (array_keys($data) as $field) {
            $fields[] = $field .'=?';
        }
        
        $entity = $validator->getEntity();

        if ($entity->getId()) {
            $sql = 'UPDATE ' . $this->table . ' SET ' . implode(',', $fields) . ' WHERE id=?';
            $data['id'] = $entity->getId();
        } else {
            $sql = 'INSERT INTO ' . $this->table . ' SET ' . implode(',', $fields);
        }

        $stmt = $this->db->prepare($sql);
        $key = 1;
        foreach ($data as $value) {
            if ('NULL' === $value) {
                $stmt->bindValue($key++, null,  \PDO::PARAM_NULL);
            } else {
                $stmt->bindValue($key++, $value);
            }
        }
        
        $stmt->execute();

        if (!$entity->getId()) {
            $entity->setId($this->db->lastInsertId());
        }

        return $entity;
    }

    public function delete(Entity $entity)
    {
        $return = true;

        if (method_exists($this, 'deleteLinks')) {
            $return &= $this->deleteLinks($entity);
        }

        $sql = 'DELETE FROM ' . $this->table . ' WHERE id=?';

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(1, $entity->getId(), \PDO::PARAM_INT);

        $return &= $stmt->execute();

        return $return;
    }
}