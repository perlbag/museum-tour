<?php

namespace Validators;

use Tools\Validator;

class Museum extends Validator
{
    protected $allowed = ['label', 'description'];
    protected $required = ['label'=>'Label is required'];
}