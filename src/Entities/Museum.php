<?php

namespace Entities;

use Tools\Entity;
use Tools\Db;

class Museum extends Entity
{
	protected $id;
	protected $label;
	protected $description;

    public function toArray()
    {
        $data = parent::toArray();
        $data['categories'] = $this->getCategories();

        return $data;
    }

    public function getCategories()
    {
        $db = Db::getInstance();
        
        $sql = 'SELECT category_id FROM museum_has_category WHERE museum_id=?';

        $stmt = $db->prepare($sql);
        $stmt->bindValue(1, $this->getId(), \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_COLUMN, 0);
    }
}