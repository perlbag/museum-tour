<?php

namespace Controllers\Backend;

use Tools\Controller;
use Tools\View;
use Tools\Response;

use Validators\Museum as Validator;

class Museum extends Controller
{
    public function indexAction()
    {
        $list = $this->getFactory()->getAll();

        $view = new View('backend/museum/index', ['list'=>$list]);
        $view->setLayout('backend');

        $response = new Response();
        $response->setBody($view->render());

        return $response;
    }

    public function createAction()
    {
        return $this->form($this->getFactory()->create());
    }

    public function updateAction()
    {
        return $this->form($this->getEntity());
    }

    public function deleteAction()
    {
        $entity = $this->getEntity();

        $this->getFactory()->delete($entity);

        return $this->redirect('/backend/museum');
    }

    protected function form($entity)
    {
        $vars = $entity->toArray();
        
        $errors = [];
        
        if ('POST' === $_SERVER['REQUEST_METHOD']) {
            $vars = array_merge($vars, $_POST['museum']);
            $validator = new Validator($vars, $entity);
            if ($validator->isValid()) {
                $this->getFactory()->save($validator, $vars['categories']);
                return $this->redirect('/backend/museum');
            }

            $errors = $validator->getErrors();
        }
        
        $categories = new \Factories\Category();
        $tree = $categories->getTree();

        $view = new View('/backend/museum/form', ['tree'=>$tree, 'vars'=>$vars, 'errors'=>$errors]);
        $view->setLayout('backend');
        
        $response = new Response();
        $response->setBody($view->render());

        return $response;
    }
}