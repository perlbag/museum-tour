<?php

namespace Validators;

use Tools\Validator;

class Category extends Validator
{
    protected $allowed = ['parent_id', 'label', 'description'];
    protected $required = ['label'=>'Label is required'];
}