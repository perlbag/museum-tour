<?php

namespace Factories;

use Tools\Factory;

use Entities\Museum as Entity;
use Validators\Museum as Validator;

class Museum extends Factory
{
	protected $table = 'museum';

    public function save(Validator $validator, array $links=[])
    {
        $return = parent::save($validator);

        $entity = $validator->getEntity();

        $this->deleteLinks($entity);
        $this->insertLinks($entity, $links);

        return $entity;
    }

    public function insertLinks(Entity $entity, array $links)
    {
        $return = true;

        $sql = 'INSERT INTO museum_has_category (museum_id, category_id) VALUES (?,?)';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(1, $entity->getId(), \PDO::PARAM_INT);
        foreach ($links as $link) {
            $stmt->bindValue(2, $link, \PDO::PARAM_INT);
            $return &= $stmt->execute();
        }

        return $return;
    }

    public function deleteLinks(Entity $entity)
    {
        $sql = 'DELETE FROM museum_has_category WHERE museum_id=?';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(1, $entity->getId(), \PDO::PARAM_INT);

        return $stmt->execute();
    }
}