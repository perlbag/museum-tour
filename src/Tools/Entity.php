<?php

namespace Tools;

abstract class Entity
{
    public function toArray()
    {
        return get_object_vars($this);
    }

    public function __call($method, $args)
    {
        switch (true) {
            case 0 === strpos($method, 'get'):
                $key = strtolower(substr($method, 3));
                if (isset($this->$key)) {
                    return $this->$key;
                }
                break;
            case 0 === strpos($method, 'set'):
                $key = strtolower(substr($method, 3));
                $this->$key = $args[0];
                break;
        }
    }
}