<?php

namespace Controllers\Frontend;

use Tools\Controller;
use Tools\View;
use Tools\Context;
use Tools\Response;

use Factories\Museum as Factory;

class Museum extends Controller
{
    public function viewAction()
    {
        parse_str($this->context->getQuery());

        if (!isset($id)) {
            throw new \RuntimeException('Missing parameter');
        }

        $factory = new Factory();
        $entity = $factory->getById($id);

        if (null === $entity) {
            throw new \Exception('Museum not found', 404);
        }

        $view = new View('frontend/museum/view', ['item'=>$entity]);

        $response = new Response();
        $response->setBody($view->render());

        return $response;
    }
}
